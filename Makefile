export VERSION=0.0.1

.PHONY: all
all: doc

.PHONY: clean
clean:
	$(MAKE) -C doc clean

prefix ?= /usr/local
exec_prefix ?= $(prefix)
bindir ?= $(exec_prefix)/bin
sysconfdir ?= $(prefix)/etc

INSTALL ?= install

commands = $(wildcard bin/myofb-*)
inst_bindir = $(abspath $(DESTDIR)$(bindir))
inst_sysconfdir = $(abspath $(DESTDIR)$(sysconfdir))

.PHONY: install
install: bin_install doc_install conf_install

.PHONY: bin_install
bin_install:
	$(INSTALL) -d $(inst_bindir)
	for c in $(commands); do \
		$(INSTALL) $$c $(inst_bindir); \
	done
	sed -i -E \
		-e 's|%PREFIX%|$(prefix)|' \
		-e 's|%SYSCONFDIR%|$(sysconfdir)|' \
		$(inst_bindir)/myofb-preload

.PHONY: conf_install
conf_install:
	$(INSTALL) -d $(inst_sysconfdir)
	cd etc && find myofb -type d -exec $(INSTALL) -d $(inst_sysconfdir)/{} \;
	cd etc && find myofb -type f | \
		while read fn; do \
			basename "$$fn" | grep -q '^\.' && continue; \
			$(INSTALL) $$fn $(inst_sysconfdir)/$$(dirname $$fn); \
		done

.PHONY: uninstall
uninstall:
	$(RM) $(addprefix $(inst_bindir)/,$(notdir $(commands)))
	$(RM) -r $(inst_sysconfdir)/myofb
	$(MAKE) -C doc DESTDIR=$(abspath $(DESTDIR)) uninstall

.PHONY: doc
doc:
	$(MAKE) -C doc

.PHONY: doc_install
doc_install: doc
	$(MAKE) -C doc DESTDIR=$(abspath $(DESTDIR)) install
